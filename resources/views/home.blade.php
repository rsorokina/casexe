@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (isset($prize))
                        <div class="text-center mt-3">
                            <h3>{{$prize['title']}}</h3>

                            @if (!isset($prize['status']))
                            <form action="/decision" method="post">
                            {{ csrf_field() }}
                                <input type="submit" name="decision" value="accept" class="btn btn-info btn-lg">
                                <input type="submit" name="decision" value="refuse" class="btn btn-danger btn-lg">
                            </form>
                            @endif
                        </div>

                        @elseif(isset($decision))
                            <div class="text-center mt-3">
                                <h3>{{$decision}}</h3>
                            </div>

                    @else
                        <form action="/home" method="post">
                            {{ csrf_field() }}
                        <div class="text-center mt-3">
                            <button type="submit" class="btn btn-info btn-lg">
                                Get a prize!
                            </button>
                        </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
