<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Prize;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = User::find(Auth::id());

        $prize = null;
        if ($user->prize_type) {
            $prize = [
                'value' => $user->prize_value,
                'status' => $user->prize_status,
                'title' => 'Приз'
            ];
        }
        if ($request->isMethod('post')) {
            $new_prize = new Prize;
            $prize = $new_prize->getPrize();

            $user->prize_type = $prize['type'];
            $user->prize_value = $prize['value'];
            $user->save();
        }

        return view('home')->with(compact('prize'));
    }


    public function decision(Request $request)
    {
        $decision = $request->input('decision');

        $user = User::find(Auth::id());
        if ($decision == 'accept') {
            $user->prize_status = 1;
        } else {
            $user->prize_type = null;
            $user->prize_value = null;
        }
        $user->save();
        return redirect('/home');
    }
}
