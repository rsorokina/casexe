<?php

namespace App\Console\Commands;

use Complex\Exception;
use Illuminate\Console\Command;
use App\Classes\ApiBank;
use App\User;

class SendMoneyPrize extends Command
{
    private const N = 100;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'money-prize:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users_prizes = User::where('prize_type', 1)->where('prize_status', 1)->take(N)->get();
        foreach ($users_prizes as $user_prize) {
            $req = [
                'prize_value'       => $user_prize->prize_value,
                'user_card_number'  => $user_prize->user_card_number
            ];

            try {
                $res = ApiBank::sendRequest($req);
                $user_prize->prize_status = 2;
                $user_prize->save();
            } catch (Exception $e) {
                Log::alert('Pay error'. $e);
            }

        }
    }
}
