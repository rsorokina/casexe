<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use DB;

class Prize extends Model
{
    protected $prizes = [
        1 => [
            'min' => 20,
            'max' => 500,
            'amount' => 25
        ],
        2 => [
            'min' => 200,
            'max' => 3000,
            'amount' => 500
        ],
        3 => [
            'list' => ['phone', 'TV', 'car'],
            'amount' => 120
        ]
    ];

    public function getPrize()
    {
        $this->usePrizes();

        if ($this->getRandomPrize() < $this->prizes[1]['amount'])
            return $this->getMoneyPrize();
        elseif ($this->getRandomPrize() - $this->prizes[1]['amount'] < $this->prizes[2]['amount'])
            return $this->getBonusPrize();
        else return $this->getThingPrize();
    }


    private function getMoneyPrize()
    {
        $value = rand($this->prizes[1]['min'], $this->prizes[1]['max']);
        return [
            'type' => 1,
            'value' => $value,
            'title' => 'Денежный приз: ' . $value . ' у.е.'
        ];
    }

    private function getBonusPrize()
    {
        $value = rand($this->prizes[2]['min'], $this->prizes[2]['max']);
        return [
            'type' => 2,
            'value' => $value,
            'title' => 'Начислено Бонусов: ' . $value
        ];
    }


    private function getThingPrize()
    {
        $value = $this->prizes[3]['list'][array_rand($this->prizes[3]['list'])];
        return [
            'type' => 3,
            'value' => $value,
            'title' => 'Приз: ' . $value
        ];
    }


    private function getRandomPrize()
    {
        return rand(0, $this->getAmountPrize());
    }


    private function getAmountPrize()
    {
        return collect($this->prizes)->pluck('amount')->sum();
    }


    private function usePrizes() {
        $use_prizes = User::select(DB::raw('count(*) as prize_count, prize_type'))->whereNotNull('prize_status')->groupBy('prize_type')->get()->keyBy('prize_type');
        if (isset($use_prizes[1])) $this->prizes[1]['amount'] -= $use_prizes[1]->prize_count;
        if (isset($use_prizes[3])) $this->prizes[3]['amount'] -= $use_prizes[3]->prize_count;
    }
}
